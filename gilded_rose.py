# -*- coding: utf-8 -*-
#from abc import abstractmethod
from abc import abstractmethod

MAX_QUALITY: int = 50
SULFURAS: str = "Sulfuras, Hand of Ragnaros"
AGED_BRIE: str = "Aged Brie"
BACKSTAGE_PASSES: str = "Backstage passes to a TAFKAL80ETC concert"
CONJURED: str = "Conjured"


class GildedRose(object):

    def __init__(self, items):
        self.items = items

    def update(self):
        item: Item

        for item in self.items:
            item.update()

            self.check_if_sulfras(item)

            self.update_brie(item)

            self.update_backstage_passes(item)

            self.update_conjured(item)

    def update_conjured(self, item):
        # Update if the item is Conjured
        if CONJURED == item.name:
            item.sell_in -= 1
            item.quality -= 2

    def update_backstage_passes(self, item):
        # Update if the item is Backstage Passes
        if BACKSTAGE_PASSES == item.name:
            item.sell_in -= 1

            if item.quality < MAX_QUALITY:
                item.quality += 1
                if item.sell_in < 11:
                    if item.quality < MAX_QUALITY:
                        item.quality += 1
                if item.sell_in < 6:
                    if item.quality < MAX_QUALITY:
                        item.quality += 1

            if item.sell_in < 0:
                item.quality = 0

    def update_brie(self, item):
        # Update if the item is Aged Brie
        if AGED_BRIE == item.name:
            item.sell_in -= 1
            if item.sell_in > 0:
                item.quality += 1
            else:
                item.quality += 2
            if item.quality > MAX_QUALITY:
                item.quality = MAX_QUALITY

    def check_if_sulfras(self, item):
        # Update if the item is Sulfuras
        if SULFURAS == item.name:
            pass

class Item:
    name: str
    sell_in: int
    quality: int

    def __init__(self, name, sell_in, quality):
        self.name = name
        self.sell_in = sell_in
        self.quality = quality

    def __repr__(self):
        return "%s, %s, %s" % (self.name, self.sell_in, self.quality)

    @abstractmethod
    def update(self):
        pass

class Normal_Item(Item):
    name: str
    sell_in: int
    quality: int

    def update(self):
        # Update if the item is a normal item
        if not (self.name == AGED_BRIE or BACKSTAGE_PASSES == self.name or SULFURAS == self.name
                or CONJURED == self.name):
            self.sell_in -= 1
            if self.sell_in > 0:
                self.quality -= 1
            else:
                self.quality -= 2
            if self.quality < 0:
                self.quality = 0



